interface Video {
    videoID: string;
    title?: string;
    type?: string;
    aspect?: string;
}

interface TeamMember {
    name?: string;
    firstName: string;
    lastName: string;
    role?: string;
    description?: string;
    thumbnail?: string;
}

interface solutionItem {
    _path?: string;
    img?: string;
    title: string;
    description?: string;
    tags?: string[];
}

interface cardItem {
    _path?: string;
    img?: string;
    title: string;
    date?: string;
    description?: string;
}