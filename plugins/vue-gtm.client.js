import {createGtm} from "@gtm-support/vue-gtm";

export default defineNuxtPlugin((nuxtApp) => {
    const runtimeConfig = useRuntimeConfig().public;

    const gtm = createGtm({
        id: runtimeConfig.googleTagManagerId,
        // queryParams: {
        //     // Add URL query string when loading gtm.js with GTM ID (required when using custom environments)
        //     gtm_auth: 'AB7cDEf3GHIjkl-MnOP8qr',
        //     gtm_preview: 'env-4',
        //     gtm_cookies_win: 'x',
        // },
        // source: 'gtm.js', // add custom source (default: gtm.js)
        defer: false,
        compatibility: false,
        // nonce: '2726c7f26c'
        enabled: true,
        debug: runtimeConfig.environment === 'development',
        loadScript: true,
        vueRouter: useRouter(),
        // ignoredViews: ['homepage'], // Don't trigger events for specified router names (optional)
        trackOnNextTick: false,
    });

    nuxtApp.vueApp.use(gtm);
});
