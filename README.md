# GraphPolaris Website

This is a Nuxt 3 project with Nuxt Content.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed the latest version of [Node.js](https://nodejs.org/en/download/)
- You have installed [pnpm](https://pnpm.io/installation) package manager

## Installing Project

To install Project, follow these steps in a terminal:

1. Clone the repository:
    ```bash
    git clone https://github.com/duncandejong/graph-polaris-landing-page.git
    ```

2. Navigate to the project directory:
    ```bash
    cd your-project-folder
    ```

3. Install the dependencies:
    ```bash
    pnpm install
    ```

## Using Project

To use Project, follow these steps:

1. Start the development server:
    ```bash
    pnpm dev
    ```

2. Open your browser and navigate to `http://localhost:3000`.

## Alternative: Using Docker

```bash
docker build -t graph-polaris-landing-page .
docker run --rm -p 3000:3000 graph-polaris-landing-page
```
Open your browser and navigate to `http://localhost:3000`.

## Contributing to Project

To contribute to Project, follow these steps:

1. Fork this repository.
2. If there is no branch called 'staging', create one: `git checkout -b staging`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the branch: `git push origin staging`
5. Create a pull request to the `main` branch.

Alternatively, see the GitHub documentation
on [creating a pull request](https://docs.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Working with the Project

When working with the project, it's important to regularly pull changes from the remote repository to stay up-to-date
with the latest developments. Here are the steps to do so:

1. Pull the latest changes from the remote repository:
    ```bash
    git pull origin staging
    ```

2. If there are any conflicts between your local changes and the changes on the remote repository, Git will inform you
   after the pull command. Open the conflicting files and you'll see markers that indicate the conflicting areas. It
   will look something like this:

    ```bash
    <<<<<<< HEAD
    Your changes
    =======
    Changes from the remote repository
    >>>>>>> commit_id
    ```

3. Resolve the conflicts by deciding which changes to keep. You can choose to keep your changes, the changes from the
   remote repository, or a combination of both.

4. After resolving the conflicts, add the resolved files to the staging area:
    ```bash
    git add <file_name>
    ```

5. Commit the resolved changes:
    ```bash
    git commit -m "Resolved merge conflicts"
    ```

6. Finally, push your changes to the remote repository:
    ```bash
    git push origin staging
    ```

Remember, it's important to push your changes regularly. If you wait too long, others may make conflicting changes,
making it harder to merge your changes later.
