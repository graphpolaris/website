export const useDateFormat = () => {
    const formatDate = (value: any, format = 'default', customLocale = 'en') => {
        const date = new Date(value);
        const locale = customLocale || navigator.language || 'en'; // Set 'en' as the default

        switch (format) {
            case 'monthYear':
                return date.toLocaleString([locale], {
                    month: 'short',
                    year: 'numeric',
                });
            default:
                return date.toLocaleString([locale], {
                    month: 'short',
                    day: '2-digit',
                    year: 'numeric',
                });
        }
    };

    return {formatDate};
};