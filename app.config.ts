export default defineAppConfig({
    ui: {
        primary: "brand",
        gray: "tertiary",
        secondary: "secondary",
        safelistColors: ['secondary', 'tertiary', 'brand'],
        formGroup: {
            help: "text-xs mt-1 text-tertiary-500",
            error: "text-xs mt-1 text-red-500",
            label: {
                base: "text-xl block font-medium text-tertiary-500",
            },
        },
        // icons: ["heroicons", "lucide", "ic"],
        icons: {dynamic: true},

        button: {
            base: 'focus:outline-none focus-visible:outline-0 disabled:cursor-not-allowed disabled:opacity-75 flex-shrink-0 transition-colors',
            font: 'font-medium',
            rounded: 'rounded-md',
            truncate: 'text-left break-all line-clamp-1',
            block: 'w-full flex justify-center items-center',
            inline: 'inline-flex items-center',
            size: {
                '2xs': 'text-xs',
                xs: 'text-xs',
                sm: 'text-sm',
                md: 'text-sm',
                lg: 'text-sm',
                xl: 'text-base',
            },
            gap: {
                '2xs': 'gap-x-1',
                xs: 'gap-x-1.5',
                sm: 'gap-x-1.5',
                md: 'gap-x-2',
                lg: 'gap-x-2.5',
                xl: 'gap-x-2.5',
            },
            padding: {
                '2xs': 'px-2 py-1',
                xs: 'px-2.5 py-1.5',
                sm: 'px-2.5 py-1.5',
                md: 'px-3 py-2',
                lg: 'px-3.5 py-2.5',
                xl: 'px-3.5 py-2.5',
            },
            square: {
                '2xs': 'p-1',
                xs: 'p-1.5',
                sm: 'p-1.5',
                md: 'p-2',
                lg: 'p-2.5',
                xl: 'p-2.5',
            },

            variant: {
                solid: 'shadow text-light dark:text-light bg-{color}-600 hover:bg-{color}-500 disabled:bg-tertiary-300 dark:bg-{color}-500 dark:hover:bg-{color}-400 dark:disabled:bg-tertiary-300 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-{color}-500 dark:focus-visible:outline-{color}-500',
                outline: 'ring-1 ring-inset ring-current text-{color}-600 dark:text-{color}-600 hover:bg-{color}-100 disabled:bg-transparent dark:hover:bg-{color}-100 dark:disabled:bg-transparent focus-visible:ring-2 focus-visible:ring-{color}-500 dark:focus-visible:ring-{color}-500',
                soft: 'text-{color}-600 hover:text-{color}-700 dark:text-{color}-500 dark:hover:text-{color}-600 bg-opacity-10 dark:bg-opacity-10 hover:bg-opacity-20 dark:hover:bg-opacity-20 bg-{color}-400 hover:bg-{color}-400 dark:bg-{color}-500 dark:hover:bg-{color}-500 disabled:bg-{color}-50 dark:disabled:bg-{color}-950 focus-visible:ring-2 focus-visible:ring-inset focus-visible:ring-{color}-500 dark:focus-visible:ring-{color}-500',
                ghost: 'text-{color}-600 hover:text-{color}-700 dark:text-{color}-500 dark:hover:text-{color}-600 bg-opacity-0 dark:bg-opacity-0 hover:bg-opacity-20 dark:hover:bg-opacity-20 bg-{color}-400 hover:bg-{color}-400 dark:bg-{color}-500 dark:hover:bg-{color}-500 disabled:bg-transparent dark:disabled:bg-transparent focus-visible:ring-2 focus-visible:ring-inset focus-visible:ring-{color}-500 dark:focus-visible:ring-{color}-500',
                link: 'text-{color}-600 hover:text-{color}-500 disabled:text-{color}-500 dark:text-{color}-600 dark:hover:text-{color}-500 dark:disabled:text-{color}-400 underline-offset-4 hover:underline focus-visible:ring-2 focus-visible:ring-inset focus-visible:ring-{color}-500 dark:focus-visible:ring-{color}-500',
            },
            icon: {
                base: 'flex-shrink-0',
                loading: 'animate-spin',
                size: {
                    '2xs': 'h-4 w-4',
                    xs: 'h-4 w-4',
                    sm: 'h-5 w-5',
                    md: 'h-5 w-5',
                    lg: 'h-5 w-5',
                    xl: 'h-6 w-6',
                },
            },
            default: {
                size: 'md',
                variant: 'solid',
                color: 'primary',
                loadingIcon: 'i-heroicons-arrow-path-20-solid',
            },
        },
        modal: {
            wrapper: 'relative z-50',
            width: 'w-full sm:max-w-2xl',
            overlay: {
                background: "backdrop",
            },
            background: "bg-light dark:bg-dark",
            container: 'flex min-h-full items-center justify-center text-center',
            padding: "p-4 sm:p-0",
            rounded: "rounded-t-2xl sm:rounded-xl",
            transition: {
                enterFrom: "opacity-0 translate-y-full sm:translate-y-0 sm:scale-x-95",
                leaveFrom: "opacity-100 translate-y-0 sm:scale-x-100",
            },
        },
        card: {
            base: '',
            background: 'bg-light dark:bg-dark',
            divide: 'divide-y divide-tertiary-200 dark:divide-tertiary-800',
            ring: 'ring-1 ring-tertiary-200 dark:ring-tertiary-800',
            rounded: 'rounded-lg',
            shadow: 'shadow',
            body: {
                base: '',
                background: '',
                padding: 'px-4 py-5 sm:p-6',
            },
            header: {
                base: '',
                background: '',
                padding: 'px-4 py-5 sm:px-6',
            },
            footer: {
                base: '',
                background: '',
                padding: 'px-4 py-4 sm:px-6',
            },
        },
        tooltip: {
            wrapper: 'relative inline-flex',
            container: 'z-20 group',
            width: 'max-w-xs',
            background: 'bg-tertiary-100 dark:bg-tertiary-900',
            color: 'text-gray-900 dark:text-white',
            shadow: 'shadow',
            rounded: 'rounded',
            ring: 'ring-1 ring-tertiary-200 dark:ring-tertiary-800',
            base: '[@media(pointer:coarse)]:hidden h-6 px-2 py-1 text-xs font-normal truncate relative',
            shortcuts: 'hidden md:inline-flex flex-shrink-0 gap-0.5',
            middot: 'mx-1 text-gray-700 dark:text-gray-200',
            transition: {
                enterActiveClass: 'transition ease-out duration-200',
                enterFromClass: 'opacity-0 translate-y-1',
                enterToClass: 'opacity-100 translate-y-0',
                leaveActiveClass: 'transition ease-in duration-150',
                leaveFromClass: 'opacity-100 translate-y-0',
                leaveToClass: 'opacity-0 translate-y-1',
            },
            popper: {
                strategy: 'fixed',
            },
            default: {
                openDelay: 0,
                closeDelay: 0,
            },
            arrow: {
                base: '[@media(pointer:coarse)]:hidden invisible before:visible before:block before:rotate-45 before:z-[-1] before:w-2 before:h-2',
                ring: 'before:ring-1 before:ring-gray-200 dark:before:ring-gray-800',
                rounded: 'before:rounded-sm',
                background: 'before:bg-gray-200 dark:before:bg-gray-800',
                shadow: 'before:shadow',
                placement: "group-data-[popper-placement*='right']:-left-1 group-data-[popper-placement*='left']:-right-1 group-data-[popper-placement*='top']:-bottom-1 group-data-[popper-placement*='bottom']:-top-1",
            },

        },
        container: {
            constrained: "max-w-2xl",
        },
    },
    icon: {
        // size: '2em',  // Default size
        class: 'icon',
        mode: 'svg', // default <NuxtIcon> mode applied other option 'css' is default
        // aliases: {
        //     graphPolaris: 'GraphPolaris',
        // }
    },
    mainNav: [
        {
            label: "Why GraphPolaris",
            to: "/",
            subItems: [
                {
                    label: "Our Product",
                    description: "",
                    to: "/#product"
                },
                {
                    label: "Our Approach",
                    description: "",
                    to: "/#approach"
                },
            ]
        },
        {
            label: "Solutions",
            to: "/solutions",
            subItems: [
                {
                    label: "Asset Performance Management",
                    description: "",
                    to: "/solutions/asset-performance-management"
                },
                {
                    label: "Security & Intelligence",
                    description: "",
                    to: "/solutions/security-intelligence"
                },
                {
                    label: "Healthcare & Personalized Pathways",
                    description: "",
                    to: "/solutions/healthcare-personalized-pathways"
                },
            ],
        },
        {
            label: "Resources",
            to: "/resources",
            subItems: [
                // {
                //     label: "Blog",
                //     description: "",
                //     to: "/resources/blog"
                // },
                // {
                //     label: "Case Studies",
                //     description: "",
                //     to: "/resources/case-studies"
                // },
                {
                    label: "News & Events",
                    description: "",
                    to: "/resources/news"
                },
                {
                    label: "Videos",
                    description: "",
                    to: "/resources/videos"
                },
                // {
                //     label: "Our Impact",
                //     description: "",
                //     to: "/resources/our-impact"
                // },

            ]
        },
        {
            label: "Company",
            to: "/company/about-us",
            subItems: [
                {
                    label: "About Us",
                    description: "",
                    to: "/company/about-us"
                },
                {
                    label: "Our Team",
                    description: "",
                    to: "/company/team"
                },
                {
                    label: "Investors",
                    description: "",
                    to: "/company/investors"
                },
                {
                    label: "Careers",
                    description: "",
                    to: "/company/careers"
                },
                {
                    label: "Contact",
                    description: "",
                    to: "/contact"
                }
            ]
        },
    ],
    footerNav: [
        {
            title: 'General',
            links: [{
                label: 'Pricing',
                to: '/contact'
            }, {
                label: 'Privacy Policy',
                to: '/privacy-policy'
            }, {
                label: 'Terms and Conditions',
                to: '/terms'
            }]
        },

        {
            title: 'Solutions',
            links: [
                {
                    label: "Asset Performance Management",
                    to: "/solutions/asset-performance-management"
                },
                {
                    label: "Security & Intelligence",
                    to: "/solutions/security-intelligence"
                },
                {
                    label: "Healthcare & Personalized Pathways",
                    to: "/solutions/healthcare-personalized-pathways"
                },
            ]
        },
        {
            title: 'Resources',
            links: [
                // {
                //     label: 'Blog',
                //     to: '/resources/blog'
                // },
                // {
                //     label: "Case Studies",
                //     description: "",
                //     to: "/resources/case-studies"
                // },
                {
                    label: 'News & Events',
                    to: '/resources/news'
                },
                {
                    label: 'Videos',
                    to: '/resources/videos'
                },
                // {
                //     label: 'Our Impact',
                //     to: '/resources/our-impact'
                // }
            ]
        },
        {
            title: 'Company',
            links: [
                {
                    label: "About Us",
                    to: "/company/about-us"
                },
                {
                    label: "Our Team",
                    to: "/company/team"
                },
                {
                    label: "Our Investors",
                    to: "/company/investors"
                },
                {
                    label: "Careers",
                    to: "/company/careers"
                },
                {
                    label: "Contact",
                    to: "/contact"
                }
            ]
        },
    ],
});
