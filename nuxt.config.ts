//https://nuxt.com/docs/api/nuxt-config
export default defineNuxtConfig({
    devtools: {
        enabled: process.env.NUXT_SITE_ENV === "development",
        timeline: {
            enabled: true,
        },
    },
    // https://nuxt.com/docs/guide/concepts/rendering
    // ssr: false,
    nitro: {
        output: {
            publicDir: "nuxt-graphpolaris.com",
        }
    },
    compatibilityDate: {
        cloudflare: "2024-07-12"
    },
    modules: [
        "@nuxt/ui",
        "@nuxt/icon",
        "@nuxtjs/google-fonts",
        "@nuxt/image",
        "@nuxt/content",
        "@vueuse/nuxt",
        'radix-vue/nuxt',
        "@nuxtjs/tailwindcss",
        "nuxt-particles",
        'nuxt-swiper',
        "@nuxtjs/seo",
        "@dargmuesli/nuxt-cookie-control",
        "@nuxtjs/color-mode",
        // "nitro-cloudflare-dev",
    ],
    colorMode: {
        preference: 'system', // default value of $colorMode.preference
        fallback: 'light', // fallback value if not system preference found
        hid: 'nuxt-color-mode-script',
        globalName: '__NUXT_COLOR_MODE__',
        componentName: 'ColorScheme',
        classPrefix: '',
        classSuffix: '-mode',
        storageKey: 'nuxt-color-mode'
    },
    cookieControl: {
        // Position of cookie bar.
// 'top-left', 'top-right', 'top-full', 'bottom-left', 'bottom-right', 'bottom-full'
        barPosition: 'bottom-full',

// Switch to toggle if clicking the overlay outside the configuration modal closes the modal.
        closeModalOnClickOutside: true,

// Component colors.
// If you want to disable colors set colors property to false.
        colors: false,

// The cookies that are to be controlled.
// See detailed explanation further down below!
        cookies: {
            necessary: [
                {
                    id: 'n',
                    name: {
                        de: 'Notwendiger Cookie',
                        en: 'Necessary Cookie',
                        nl: 'Noodzakelijke Cookie',
                    },
                    description: {
                        de: 'Um Ihre Cookie-Einstellungen zu speichern.',
                        en: 'Needed to store your cookie preferences.',
                        nl: 'Nodig om uw cookievoorkeuren op te slaan.',
                    },
                    targetCookieIds: ['NEC'],
                },
            ],
            optional: [
                {
                    id: 't',
                    name: {
                        de: 'Drittanbieter-Cookie',
                        en: 'Third party Cookie',
                        nl: 'Third party Cookie',
                    },
                    description: {
                        de: 'Mit diesem Cookie können Sie (YouTube) Videos ansehen.',
                        en: 'With this cookie you can view (YouTube) videos.',
                        nl: 'Met deze cookkie kun je (YouTube) video\'s bekijken.',
                    },
                    links: {
                        '/privacy-policy': 'Privacy Policy',
                    },
                    targetCookieIds: ['_t'],
                },
                {
                    id: 'a',
                    name: {
                        de: 'Analytischer Cookie',
                        en: 'Analytical Cookie',
                        nl: 'Analytische Cookie',
                    },
                    description: {
                        de: 'Damit können wir die Nutzung der Website messen und verbessern.',
                        en: 'With this we can measure and improve the use of the website.',
                        nl: 'Hiermee kunnen we het gebruik van de website meten en verbeteren.',
                    },
                    targetCookieIds: ['_ga'],
                },
            ],
        },

// The milliseconds from now until expiry of the cookies that are being set by this module.
        cookieExpiryOffsetMs: 1000 * 60 * 60 * 24 * 365, // one year

// Names for the cookies that are being set by this module.
        cookieNameIsConsentGiven: 'ncc_c',
        cookieNameCookiesEnabledIds: 'ncc_e',

// Options to pass to nuxt's useCookie
        cookieOptions: {
            path: '/',
            sameSite: 'strict',
        },

// Switch to toggle the "accept necessary" button.
        isAcceptNecessaryButtonEnabled: false,

// Switch to toggle the button that opens the configuration modal.
        isControlButtonEnabled: true,

// Switch to toggle the concatenation of target cookie ids to the cookie description.
        isCookieIdVisible: false,

// Switch to toggle the inclusion of this module's css.
// If css is set to false, you will still be able to access your color variables.
        isCssEnabled: true,

// Switch to toggle the css variables ponyfill.
        isCssPonyfillEnabled: false,

// Switch to toggle the separation of cookie name and description in the configuration modal by a dash.
        isDashInDescriptionEnabled: true,

// Switch to toggle the blocking of iframes.
// This can be used to prevent iframes from adding additional cookies.
        isIframeBlocked: false,

// Switch to toggle the modal being shown right away, requiring a user's decision.
        isModalForced: false,

// The locales to include.
        locales: ['en', 'nl'],
// Translations to override.

        localeTexts: {
            en: {
                save: 'Save',
            }
        },
    },
    swiper: {
        // Swiper options
        //----------------------
        // prefix: 'Swiper',
        // styleLang: 'css',
        // modules: ['navigation', 'pagination'], // all modules are imported by default
    },
    site: {
        url: 'https://www.graphpolaris.com',
        name: 'GraphPolaris',
        exclude: ["/404"],
        description: '',
        defaultLocale: "en",
        defineOgImage: true,
    },
    ogImage: {
        fonts: [
            // will load the Noto Sans font from Google fonts
            'Poppins:400',
            'Poppins:600'
        ],
        defaults: {
            width: 1200,
            height: 630,
            emojis: 'noto',
            renderer: 'satori',
            component: 'BlogPost',
            cacheMaxAgeSeconds: 60 * 60 * 24 * 3
        }
    },

    app: {
        pageTransition: {name: "page", mode: "out-in"},
        head: {
            htmlAttrs: {
                lang: "en",
                class: "h-full",
            },
            bodyAttrs: {
                class: "bg-tertiary-50 dark:bg-tertiary-900 text-tertiary-900 dark:text-light min-h-screen",
            },
            charset: "utf-8",
            viewport: "width=device-width, initial-scale=1",
            link: [
                {
                    rel: "icon",
                    type: "image/png",
                    sizes: "16x16",
                    href: "/favicon/favicon-16x16.png",
                },
                {
                    rel: "icon",
                    type: "image/png",
                    sizes: "32x32",
                    href: "/favicon/favicon-32x32.png",
                },
                {
                    rel: "icon",
                    type: "image/png",
                    sizes: "96x96",
                    href: "/favicon/favicon-96x96.png",
                },
                {
                    rel: "icon",
                    type: "image/svg+xml",
                    href: "/favicon/favicon.svg",
                },
                {
                    rel: "apple-touch-icon",
                    sizes: "180x180",
                    href: "/favicon/apple-touch-icon.png",
                },
                {
                    rel: "mask-icon",
                    href: "favicon/safari-pinned-tab.svg",
                    color: "#FB7B04",
                },
                {
                    rel: "manifest",
                    href: "/favicon/site.webmanifest",
                },
            ],
            meta: [
                {
                    name: "msapplication-config",
                    content: "/favicon/browserconfig.xml",
                },
                {
                    name: "theme-color",
                    content: "#FB7B04",
                },
                {
                    name: "msapplication-TileColor",
                    content: "#FB7B04",
                },
            ],
        },
    },
    content: {
        documentDriven: true,
        markdown: {
            toc: {depth: 4, searchDepth: 4}
        },
        highlight: {
            theme: {
                // Default theme (same as single string)
                default: 'night-owl',
                // Theme used if `html.dark`
                dark: 'material-theme-ocean',
            }
        },
    },
    runtimeConfig: {
        // Keys within public, will be also exposed to the client-side
        public: {
            googleTagManagerId: "GTM-5QMNR6FP",
        },
    },
    icon: {
        componentName: 'NuxtIcon'
    },
    particles: {
        mode: 'full' // 'full' | 'slim' | 'basic' | 'custom'
    },
    css: ["@/assets/css/main.scss"],
    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        }
    },

    googleFonts: {
        display: "swap", // 'auto' | 'block' | 'swap' | 'fallback' | 'optional',
        families: {
            Inter: [300, 400, 500, 600, 700],
            Poppins: [300, 400, 500, 600, 700],
        },
    },
});