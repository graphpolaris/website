---
title: Legal Information
description: Legal details about GraphPolaris B.V. and GraphPolaris Holdings B.V., including registration, contact information, and financial identifiers
date: 2025-01-01
img:
layout: default
toc: false
_draft: false
---

::hero
Legal Information
#lead
GraphPolaris B.V. & GraphPolaris Holdings B.V.
::

## Registered Office Address

**GraphPolaris (Holdings) B.V.**  
Ambachtsweg 1a,  
3953BZ MaarsbergenThe  
Netherlands

### Contact Information

- **Email:** [info@graphpolaris.com](mailto:info@graphpolaris.com)
- **Telephone:** [+31 6 13 45 37 82](tel:+31613453782)

### Company Registration & VAT Information (BTW-ID)

| Company                        | Registration (KVK Number) | VAT ID (BTW-ID) |
|--------------------------------|---------------------------|-----------------|
| **GraphPolaris B.V.**          | 93305362                  | NL866347860B01  |
| **GraphPolaris Holdings B.V.** | 93300131                  | NL866345656B01  |

### Bank Account Information

**IBAN:** NL18 RABO 0189 4969 91  
**BIC:** RABONL2U

This page serves to provide legal and regulatory information about GraphPolaris B.V. and GraphPolaris Holdings B.V. in
compliance with Dutch business regulations. For further inquiries, please contact us
at [info@graphpolaris.com](mailto:info@graphpolaris.com).