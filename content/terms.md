---
title: Terms and Conditions
description:
date: 2024-10-07
img:
layout: default
toc: false
_draft: false
---

::hero
Terms and Conditions
#lead
Our Terms & Conditions are stated in accordance to the NLdigital Terms.
::

Please read the NLdigital Terms ©2020 by clicking on [this link](/files/NLdigital-Terms-and-Conditions.pdf){target="_
blank" download="NLdigital-Terms-and-Conditions.pdf"}.