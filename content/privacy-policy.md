---
title: Privacy Policy
description:
date: 2025-01-08
img:
layout: default
toc: false
_draft: false
---

::hero
Our Privacy Policy
#lead
GraphPolaris is part of the GraphPolaris Holdings BV. This privacy policy will explain how our organization uses the
personal data we collect from you when you use our website.
::

## Company Information

#### Full Registered Name and Trading Name

GraphPolaris B.V. (Registered Trademark)  
GraphPolaris Holdings B.V. (Registered Trademark)

#### GraphPolaris (Holdings) B.V.

Ambachtsweg 1a  
3953BZ Maarsbergen  
The Netherlands

#### Contact Information

- Email: [info@graphpolaris.com](mailto:info@graphpolaris.com)
- Telephone: +31 6 13 45 37 82

## What data do we collect?

GraphPolaris collects the following data:

* Personal identification information (Name, email address, etc.)

## How do we collect your data?

You directly provide GraphPolaris with most of the data we collect. We collect data and process data when you:

* Register online or place an order for any of our products or services.
* Voluntarily complete a customer survey or provide feedback on any of our message boards or via email.
* Use or view our website via your browser's cookies.
* Use the GraphPolaris analytics services.

GraphPolaris may also receive your data indirectly from the following sources:

* Associated advertising and marketing services

## How will we use your data?

GraphPolaris collects your data so that we can:

* Process your order, manage your account.
* Email you with special offers on other products and services we think you might like.
* Provide personalized analytics

If you agree, GraphPolaris will share your data with our partner companies so that they may offer you their products and
services.

* none yet - (March 2024)

When GraphPolaris processes your order, it may send your data to, and also use the resulting information from, credit
reference agencies to prevent fraudulent purchases.

## How do we store your data?

Your data is securely stored on our servers, with access limited to authorized personnel. We implement industry-standard
security practices to prevent unauthorized access. GraphPolaris securely stores your data at company owned servers in
The Netherlands, EU.

GraphPolaris will keep your data for ten years. Once this time period has expired, we will delete your data by securely
erasing our server's hard discs through data erasure algorithms, such as US DoD 5220.22-M, NIST, NATO, British HMG IS5;
German Standard VSITR.

## Google OAuth

GraphPolaris application uses Google OAuth to authenticate users and access necessary account information. We request
only the minimum scope of data required for functionality. We utilize the data obtained through Google OAuth solely for
providing better user experience and enhancing our services. We respect your privacy and are committed to safeguarding
your information.

## Marketing

GraphPolaris would like to send you information about products and services of ours that we think you might like, as
well as those of our partner companies.

* none yet - (March 2024)

If you have agreed to receive marketing, you may always opt out at a later date. You have the right at any time to stop
GraphPolaris from contacting you for marketing purposes or giving your data to other members of the GraphPolaris
Holdings Group. If you no longer wish to be contacted for marketing purposes, please send us an email to
info@graphpolaris.com, subject: I no longer wish to be contacted for marketing purposes.

## Google analytics

We use Google Analytics for anonymized website traffic analysis. In order to track your session usage, Google drops a
cookie (\_ga) with a randomly-generated ClientID in your browser. This ID is anonymized and contains no identifiable
information like IP address, email, phone number, name etc. We use GA to track aggregated website behavior, such as what
pages you looked at, for how long, and so on. This information is important to us for improving the user experience and
determining site effectiveness. If you would like to access what browsing information we have - or ask us to delete any
GA data - please delete your \_ga cookies, reach out to us, and/or install the Google Analytics Opt-Out Browser Add-On.

## What are your data protection rights?

GraphPolaris would like to make sure you are fully aware of all of your data protection rights. Every user is entitled
to the following:

* **The right to access:** You have the right to request GraphPolaris for copies of your personal data. We may charge
  you a small fee for this service.

* **The right to rectification:** You have the right to request that GraphPolaris correct any information you believe is
  inaccurate. You also have the right to request GraphPolaris to complete information you believe is incomplete.

* **The right to erasure:** You have the right to request that GraphPolaris erase your personal data, under certain
  conditions.

* **The right to restrict processing:** You have the right to request that GraphPolaris restrict the processing of your
  personal data, under certain conditions.

* **The right to object to processing:** You have the right to object to GraphPolaris' processing of your personal data,
  under certain conditions.

* **The right to data portability:** You have the right to request that GraphPolaris transfer the data that we have
  collected to another organization, or directly to you, under certain conditions.

If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please
contact us at our email: info@graphpolaris.com.

## What are cookies?

Cookies are text files placed on your computer to collect standard Internet log information and visitor behavior
information. When you visit our websites, we may collect information from you automatically through cookies or similar
technology.

For further information, visit allaboutcookies.org.

## How do we use cookies?

GraphPolaris uses cookies in a range of ways to improve your experience on our website, including:

* Keeping you signed in
* Understanding how you use our website
* Improve analytics capabilities

## What types of cookies do we use?

There are a number of different types of cookies, however, our website uses:

* Functionality - GraphPolaris uses these cookies so that we recognize you on our website and remember your previously
  selected preferences. These could include what language you prefer and location you are in. A mix of first-party and
  third-party cookies are used.

* Advertising - GraphPolaris uses these cookies to collect information about your visit to our website, the content you
  viewed, the links you followed and information about your browser, device, and your IP address. GraphPolaris sometimes
  shares some limited aspects of this data with third parties for advertising purposes. We may also share online data
  collected through cookies with our advertising partners. This means that when you visit another website, you may be
  shown advertising based on your browsing patterns on our website.

* Analytics - GraphPolaris tracks user operations on public, e.g., demo datasets to improve analytics operations.

## How to manage cookies

You can set your browser not to accept cookies, and the above website tells you how to remove cookies from your browser.
However, in a few cases, some of our website features may not function as a result.

## Privacy policies of other websites

The GraphPolaris website contains links to other websites. Our privacy policy applies only to our website, so if you
click on a link to another website, you should read their privacy policy.

## Changes to our privacy policy

GraphPolaris keeps its privacy policy under regular review and places any updates on this web page. This privacy policy
was last updated on 10 March 2024.

## How to contact us

If you have any questions about GraphPolaris' privacy policy, the data we hold on you, or you would like to exercise one
of your data protection rights, please do not hesitate to contact us.

Email us at: info@graphpolaris.com

## How to contact the appropriate authority

Should you wish to report a complaint or if you feel that GraphPolaris has not addressed your concern in a satisfactory
manner, you may contact the Information Commissioner's Office (Authoriteit Persoongegevens).
Website: [https://www.autoriteitpersoonsgegevens.nl/](https://www.autoriteitpersoonsgegevens.nl/) Address: Postbus
93374, 2509 AJ Den Haag