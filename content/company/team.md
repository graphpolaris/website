---
title: Our Team
description: We are a diverse, close-knit team with a broad field of knowledge and expertise in and outside Data Science and Visualization Design.
layout: team
---

::hero
Our Team

#lead
We are a diverse, close-knit team with a broad field of knowledge and expertise in and outside Data Science and
Visualization Design.
::

Our passionate team lives at the intersection between Graph-based Visual Analytics research and the application of graph
analytics in real-world settings. GraphPolaris thrives in letting the community's amazing research results flourish in a
commercial setting.

Our expertise in visualization design and interactive data analysis allows us to offer a new take on your data and
cherry-pick the best solutions for the complex analytic problems in your industry.