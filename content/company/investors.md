---
title: Investors
description: Our investors support GraphPolaris in achieving the set objectives and developing beyond them.
layout: default
---

::hero
Investors
::

## LUMO Labs

![Lumo Labs](/images/company/logo-lumo-labs.svg){height=60}

LUMO Labs operates as an impact-driven venture capital fund that supports startups. They offer coaching programs for
founding teams in different areas, one of which is the AI/Data area. As LUMO Labs focuses on the positive impact that
emerging technologies can create both for the environment and the society, it directs GraphPolaris in assessing the
impact we can generate in each of the industry domains.

## ROM Utrecht Region

![ROM Utrecht](/images/company/logo-rom-utrecht.svg){height=60}

ROM Utrecht Region is the regional development company that stimulates innovation and sustainable growth in the province
of Utrecht and the Gooi en Vechtstreek region. It supports companies, startups, and scale-ups by investing in innovative
organizations. It helps companies, governments, and knowledge institutions meet to innovate and grow. It also offers
various services aimed at internationalizing companies. With an extensive network and expertise, it contributes to a
competitiv region where health, sustainability, and digitalization are central and where innovation flourishes.