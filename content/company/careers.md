---
title: Careers
description: Want to join the team? Discover your opportunities in GraphPolaris!
layout: default
toc: false
---

# Careers

## Why working with us will be fulfilling

We are searching for **engaged personnel who strives with us to remove the frustration and uncertainty out of the
analysis of complex datasets**. We firmly believe that data analysis processes should be (more) centered around the
person who does the analysis or makes the business decisions. A human-centered approach will inevitably lead to more,
better, and more sustainable innovations in business and societal settings.

## How and What

In GraphPolaris, we combine the creativity and common knowledge of the human with the number-crunching capabilities of
the machine and **make insights visible, explainable, and auditable through visual interactive interfaces**.

Our main product, the Graph Database Explorer, is the logical first manifestation of this credo: We are building a
visual graph analytics system that focuses on easy, explainable, and efficient access to (multivariate) graph databases
with better visualization support and a ready-to-use graph ML toolbox. Our unique selling points are:

1. A full-fledged, dedicated graph analysis workflow that starts with questions around data quality and schema
   consistency.
2. An intuitive query formulation in a visual query editor.
3. The ability to loop effortlessly through query-refinement and interactive exploration of better visualization
   techniques than just node-link diagrams.

### What we search for:

GraphPolaris is always on the look-out for talent who can breath our vision and understand its impact.

### Consider applying if:

- ... you have excellent **coding skills** (*must haves: web or full stack skills*) and love solving challenging and
  societally impactful problems.
- ... you have **(software) architecture skills** (*nice to haves: understanding of Kubernetes and microservice
  patterns*) to improve our scalable infrastructure even further.
- ... you are interested in **visual data analytics** (*e.g., with mapbox, deck.gl, pixi.js, d3.js*) to improve or
  design state-of-the-art visual solutions and workflows.
- ... you have worked with **graph databases** (*e.g., with Neo4j's cypher, Tigergraph's GSQL, or SPARQL*) to store and
  retrieve multivalue property graph data.

### Excited to join?

Feel free to share your open applications
to [info@graphpolaris.com](mailto:info@graphpolaris.com?subject=Open_Application) or reach out to the Founders team via
LinkedIn messaging if you have further questions or want to connect.