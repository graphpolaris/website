# Build stage
FROM node:18-alpine as builder

# Install dependencies for node-gyp and other build tools
RUN apk add --no-cache python3 make g++

# Enable corepack and prepare pnpm
RUN corepack enable \
    && corepack prepare pnpm@latest-9 --activate

# Set working directory
WORKDIR /app

# Copy package files
COPY package*.json pnpm-lock.yaml ./

# Install dependencies
RUN pnpm install --frozen-lockfile

# Copy the rest of the application
COPY . .

# Generate the static site (matching CI/CD pipeline)
RUN pnpm generate

# Production stage
FROM node:18-alpine

WORKDIR /app

# Copy only the generated static files
COPY --from=builder /app/nuxt-graphpolaris.com ./nuxt-graphpolaris.com

# Install lightweight server
RUN npm install -g serve

# Expose port
EXPOSE 3000

# Serve the static files
CMD ["serve", "-s", "nuxt-graphpolaris.com", "-l", "3000"]
